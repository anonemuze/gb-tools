﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace bankman
{
    class Program
    {
        static int kNumMons = 251;
        static int kNumUnown = 26;
        static int kNumTrainers = 66;

        static int kSizeBank = 0x4000; // 16KB
        static int kNumBanks = 0x80; // 2MB

        // base bank numbers of the sprite pointer banks, shouldn't be any pointer banks or sprites located below this!
        static int kBasePointerBank = 0x12;
        //static int kUnownPointerBank = 0x13; // removed because we now search for these banks instead
        //static int kTrainerPointerBank = 0x14;

        static Dictionary<int, string>[] banks = new Dictionary<int, string>[kNumBanks];

        static string banksPath = string.Empty;
        static int numBanks = 0;

        static bool HasBank(int bankNum)
        {
            if (Directory.Exists(banksPath))
                return File.Exists(Path.Combine(banksPath, $"BANK{bankNum:X}.bin"));

            if (!File.Exists(banksPath))
                return false;

            var info = new FileInfo(banksPath);
            return info.Length > (bankNum * 0x4000);
        }
        static BinaryReader OpenBank(int bankNum)
        {
            // If romBanksPath is a directory, open a reader to the bank file inside it
            if(Directory.Exists(banksPath))
            {
                var path = Path.Combine(banksPath, $"BANK{bankNum:X}.bin");
                if (!File.Exists(path))
                    Console.WriteLine("Bank file doesn't exist: " + path);
                return new BinaryReader(File.OpenRead(path));
            }

            // romBanksPath is a file, must be a rom image, create reader and seek to bank
            var reader = new BinaryReader(File.OpenRead(banksPath));
            reader.BaseStream.Position = bankNum * kSizeBank;
            return reader;
        }

        static void Main(string[] args)
        {
            if(args.Length != 2)
            {
                Console.WriteLine("usage: bankman <path> <output\\folder>");
                Console.WriteLine("path can either be path to ROM image, or path to directory containing bank12.bin etc");
                Console.WriteLine("after running, use 'gfx.py unlz pm2f_001.2bpp.bin' on each bin to decompress into 2bpp files");
                return;
            }
            for (int i = 0; i < kNumBanks; i++)
                banks[i] = new Dictionary<int, string>(); // first 0x12 don't actually get used, oh well

            banksPath = args[0];

            if(Directory.Exists(banksPath))
            {
                for(int i = 0; i < kNumBanks; i++)
                {
                    if (File.Exists(Path.Combine(banksPath, $"BANK{i:X}.bin")))
                        numBanks = i + 1;
                }
            }
            else
            {
                if(!File.Exists(banksPath))
                {
                    Console.WriteLine($"Input file/folder {banksPath} doesn't exist!");
                    return;
                }
                var info = new FileInfo(banksPath);
                numBanks = (int)((info.Length + kSizeBank - 1) / kSizeBank);
            }

            var outPath = args[1];
            if (!outPath.EndsWith("\\"))
                outPath += "\\";

            //var banksPath = @"C:\pkmn\Gen2\GS_Korean\GS_Korean\Pokemon\wk\setup\mons2_org\SOURCE\MONSDATA\BACKUP\990917_S\\";

            //var outPath = @"C:\pkmn\990917_S\";

            if (!Directory.Exists(outPath))
                Directory.CreateDirectory(outPath);

            int monsPointerBank = -1;
            int unownPointerBank = -1;
            int trainerPointerBank = -1;

            int monsPointerSize = 0;
            int unownPointerSize = 0;
            int trainerPointerSize = 0;

            for(int testBank = numBanks - 1; testBank >= 0; testBank--)
            {
                if (!HasBank(testBank))
                    continue;

                using (var reader = OpenBank(testBank))
                {
                    var bankPos = reader.BaseStream.Position;

                    // verify data first..
                    bool dataInvalid = false;
                    for (int i = 0; i < kNumMons; i++)
                    {
                        var MonsFrontBank = reader.ReadByte();
                        var MonsFrontAdrs = reader.ReadUInt16();
                        var MonsBackBank = reader.ReadByte();
                        var MonsBackAdrs = reader.ReadUInt16();

                        // check if empty bank...
                        if(i == 0 && MonsFrontBank == 0xFF && MonsFrontAdrs == 0xFFFF && MonsBackBank == 0xFF && MonsBackAdrs == 0xFFFF)
                        {
                            dataInvalid = true;
                            break;
                        }

                        if ((MonsFrontBank != 0xFF && (MonsFrontBank < kBasePointerBank || MonsFrontBank >= numBanks)) ||
                            (MonsBackBank != 0xFF && (MonsBackBank < kBasePointerBank || MonsBackBank >= numBanks)))
                        {
                            dataInvalid = true;
                            break;
                        }

                        if ((MonsFrontAdrs != 0xFFFF && MonsFrontAdrs >= 0x8000) ||
                            (MonsBackAdrs != 0xFFFF && MonsBackAdrs >= 0x8000))
                        {
                            dataInvalid = true;
                            break;
                        }
                    }

                    if (dataInvalid)
                        continue;

                    monsPointerBank = testBank;
                    monsPointerSize = (int)(reader.BaseStream.Position - bankPos);

                    reader.BaseStream.Position = bankPos;

                    for (int i = 0; i < kNumMons; i++)
                    {
                        var MonsFrontBank = reader.ReadByte();
                        var MonsFrontAdrs = reader.ReadUInt16();
                        var MonsBackBank = reader.ReadByte();
                        var MonsBackAdrs = reader.ReadUInt16();

                        if (MonsFrontBank != 0xFF && MonsFrontAdrs != 0xFF)
                            banks[MonsFrontBank].Add(MonsFrontAdrs, $"pmsf_{i + 1:000}");
                        if (MonsBackBank != 0xFF && MonsBackAdrs != 0xFF)
                            banks[MonsBackBank].Add(MonsBackAdrs, $"pmsb_{i + 1:000}");
                    }
                    break;
                }
            }

            // search for trainer sprite pointers before unown, as trainer sprite pointers are larger (requires more data to be valid)
            for (int testBank = numBanks - 1; testBank >= 0; testBank--)
            {
                if (!HasBank(testBank))
                    continue;
                if (testBank == monsPointerBank)
                    continue;

                using (var reader = OpenBank(testBank))
                {
                    var bankPos = reader.BaseStream.Position;

                    // verify data first..
                    bool dataInvalid = false;
                    for (int i = 0; i < kNumTrainers; i++)
                    {
                        var TrainerBank = reader.ReadByte();
                        var TrainerAdrs = reader.ReadUInt16();

                        // check if empty bank...
                        if (i == 0 && TrainerBank == 0xFF && TrainerAdrs == 0xFFFF)
                        {
                            dataInvalid = true;
                            break;
                        }

                        if (TrainerBank != 0xFF && (TrainerBank < kBasePointerBank || TrainerBank >= numBanks))
                        {
                            dataInvalid = true;
                            break;
                        }
                        if (TrainerAdrs != 0xFFFF && (TrainerAdrs >= 0x8000 || TrainerAdrs >= 0x8000))
                        {
                            dataInvalid = true;
                            break;
                        }
                    }

                    if (dataInvalid)
                        continue;

                    trainerPointerBank = testBank;
                    trainerPointerSize = (int)(reader.BaseStream.Position - bankPos);

                    reader.BaseStream.Position = bankPos;
                    for (int i = 0; i < kNumTrainers; i++)
                    {
                        var TrainerBank = reader.ReadByte();
                        var TrainerAdrs = reader.ReadUInt16();

                        if (TrainerBank != 0xFF && TrainerAdrs != 0xFF)
                            banks[TrainerBank].Add(TrainerAdrs, $"p2dl_{i + 1:000}");
                    }
                    break;
                }
            }

            for (int testBank = numBanks - 1; testBank >= 0; testBank--)
            {
                if (!HasBank(testBank))
                    continue;
                if (testBank == monsPointerBank || testBank == trainerPointerBank)
                    continue;

                using (var reader = OpenBank(testBank))
                {
                    var bankPos = reader.BaseStream.Position;

                    // verify data first..
                    bool dataInvalid = false;
                    for (int i = 0; i < kNumUnown; i++)
                    {
                        var UnknownFrontBank = reader.ReadByte();
                        var UnknownFrontAdrs = reader.ReadUInt16();
                        var UnknownBackBank = reader.ReadByte();
                        var UnknownBackAdrs = reader.ReadUInt16();

                        // check if empty bank...
                        if (i == 0 && UnknownFrontBank == 0xFF && UnknownFrontAdrs == 0xFFFF && UnknownBackBank == 0xFF && UnknownBackAdrs == 0xFFFF)
                        {
                            dataInvalid = true;
                            break;
                        }

                        if ((UnknownFrontBank != 0xFF && (UnknownFrontBank < kBasePointerBank || UnknownFrontBank >= numBanks)) ||
                            (UnknownBackBank != 0xFF && (UnknownBackBank < kBasePointerBank || UnknownBackBank >= numBanks)))
                        {
                            dataInvalid = true;
                            break;
                        }

                        if ((UnknownFrontAdrs != 0xFFFF && UnknownFrontAdrs >= 0x8000) ||
                            (UnknownBackAdrs != 0xFFFF && UnknownBackAdrs >= 0x8000))
                        {
                            dataInvalid = true;
                            break;
                        }
                    }

                    if (dataInvalid)
                        continue;

                    unownPointerBank = testBank;
                    unownPointerSize = (int)(reader.BaseStream.Position - bankPos);

                    reader.BaseStream.Position = bankPos;

                    for (int i = 0; i < kNumUnown; i++)
                    {
                        var UnknownFrontBank = reader.ReadByte();
                        var UnknownFrontAdrs = reader.ReadUInt16();
                        var UnknownBackBank = reader.ReadByte();
                        var UnknownBackAdrs = reader.ReadUInt16();

                        if (UnknownFrontBank != 0xFF && UnknownFrontAdrs != 0xFF)
                            banks[UnknownFrontBank].Add(UnknownFrontAdrs, $"pmsf_un{(char)('a' + i)}");
                        if (UnknownBackBank != 0xFF && UnknownBackAdrs != 0xFF)
                            banks[UnknownBackBank].Add(UnknownBackAdrs, $"pmsb_un{(char)('a' + i)}");
                    }
                    break;
                }
            }

            if (monsPointerBank == -1)
                Console.WriteLine("Failed to find monster sprite pointers in any bank :(");
            else
                Console.WriteLine($"Found monster sprite pointers at bank 0x{monsPointerBank:X}!");

            if (unownPointerBank == -1)
                Console.WriteLine("Failed to find unown sprite pointers in any bank :(");
            else
                Console.WriteLine($"Found unown sprite pointers at bank 0x{unownPointerBank:X}!");

            if (trainerPointerBank == -1)
                Console.WriteLine("Failed to find trainer sprite pointers in any bank :(");
            else
                Console.WriteLine($"Found trainer sprite pointers at bank 0x{trainerPointerBank:X}!");

            string unreferenced = "";

            // check for any unreferenced banks
            for (int i = 0x12; i < 0x20; i++)
            {
                if (banks[i].Count <= 0)
                    unreferenced += $"{i:X} - whole bank\r\n";
            }

            for (int i = 0; i < numBanks; i++)
            {
                if (banks[i].Count <= 0)
                    continue;

                using (var reader = OpenBank(i))
                {
                    var bankStart = reader.BaseStream.Position;

                    // have to do hacky shit like add banksize because sprite pointers are all +0x4000...
                    var fileEnd = (kSizeBank * 2);
                    if (fileEnd > (reader.BaseStream.Length + kSizeBank) - bankStart)
                        fileEnd = (int)((reader.BaseStream.Length + kSizeBank) - bankStart);

                    if (!banks[i].ContainsKey(fileEnd))
                        banks[i].Add(fileEnd, "end");

                    var keys = banks[i].Keys.ToList();
                    keys.Sort();

                    // check for any bytes that aren't pointed to
                    // (this might fail to detect bytes that are at the end of the bank...)

                    for (int pos = kSizeBank; pos < kSizeBank * 2; pos++)
                    {
                        if (bankStart + pos >= fileEnd)
                            break;

                        bool found = false;
                        for (int y = 0; y < keys.Count - 1; y++)
                        {
                            var addr = keys[y];
                            if (addr >= fileEnd)
                                break;
                            var endAddr = keys[y + 1];

                            if (pos >= addr && pos < endAddr)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            var realPos = pos - kSizeBank;

                            // skip any pointer bank bytes
                            if (i == monsPointerBank && realPos < monsPointerSize)
                                continue;
                            if (i == unownPointerBank && realPos < unownPointerSize)
                                continue;
                            if (i == trainerPointerBank && realPos < trainerPointerSize)
                                continue;

                            var offset = (i * kSizeBank) + realPos;
                            unreferenced += $"{i:X}:{realPos:X4} (0x{offset:X})\r\n";
                        }
                    }

                    if (fileEnd > kSizeBank * 2)
                        fileEnd = kSizeBank * 2;

                    for (int y = 0; y < keys.Count - 1; y++)
                    {
                        var addr = keys[y];
                        if (addr >= fileEnd)
                            break;
                        var endAddr = keys[y + 1];
                        var size = endAddr - addr;

                        reader.BaseStream.Position = bankStart + (addr - kSizeBank);
                        byte[] data = reader.ReadBytes(size);
                        File.WriteAllBytes(outPath + banks[i][addr] + ".2bpp.bin", data);
                    }
                }
            }

            if(!string.IsNullOrEmpty(unreferenced))
                File.WriteAllText(outPath + "unreferenced_bytes.txt", unreferenced);
        }
    }
}
