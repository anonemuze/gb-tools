@echo off
rem EDIT BELOW WITH PATH TO POKEYELLOW GFX.PY, COPY INTO OUTPUT FOLDER AFTER USING BANKMAN, AND RUN
FOR /r %%X IN (*.bin) DO (
   python "C:\pokeyellow-master\tools\gfx.py" unlz "%%X"
   del %%X
)