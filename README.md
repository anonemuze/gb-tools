
# isxman
This works with ISX (Intelligent System eXecutable) files, I've only tried with GameBoy ISX's but maybe other IS platforms will work too.

Made this to print out bank usages inside an ISX, then realised it'd be pretty easy to make it convert to ROM files too.

Eventually I learned it's actually pretty useful since this can work with both types of ISX, headerless & with header, while other tools seem to have trouble with headerless ISX files. (afaik ISLINK creates headerless, while ISLK creates headered files, not 100% sure though)

```
Usage: isxman [-rom] [-q] [-z] <path/to/isx>

Without any switches isxman will just print bank sizes/addresses.
-q switch will only print any oversized banks it finds (useful inside build scripts)
-rom switch will convert the isx into <path>.gb & <path>.sym files
-z switch will use zero-padding instead of FF padding, up to the next bank.
```

Ideally I'd like to make isxman's switches match up with the official CVTISX.exe switches, but sadly that file seems lost to time... if anyone has it please post on archive.org or somewhere!

# bankman
While there isn't much of GS's actual development (pre-localization) inside the leaked code, luckily there are different revisions of the BANK12/BANK13/etc files used to store the mons/unown/trainer sprites.

Most of the banks are final banks actually used in the code to build the ROM, but there's a few backups & leftovers around that have older sprites inside.

bankman can work with either a folder full of BANK12/BANK13/etc files, or a ROM file (converted, use isxman if the ROM is .ISX)

If a bank doesn't contain valid sprites it'll be skipped, instead of causing the bank extraction to crash.

```
Usage: bankman <path/to/bank/folder/or/ROM/file> <output/folder>
```
The data bankman extracts will still be compressed however, you can use pret's gfx.py inside pokemontools to decompress it (gfx.py unlz <path/to/bin>...)

I also included a bat file that can loop over all the bats and run this command for you, make sure to update it with the path to your copy of pokeyellow, and copy to bankman's output folder.
