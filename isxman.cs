﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace isxman
{
    public enum RecordType : byte
    {
        Unknown0 = 0,
        Binary = 1,
        Unknown2 = 2,
        Range = 3,
        Symbol = 4,

        Unk13 = 0x13,
        Unk14 = 0x14,

        Unk20 = 0x20,
        Unk21 = 0x21,
        Unk22 = 0x22
    }

    // Contains binary bytes
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct BinaryRecord
    {
        public byte BankNum; // apparently this can be 2 bytes if first byte == 0x80 or above, but DMG probably won't go that high
        public ushort Address;
        public ushort Length;
        // Data of size Length follows..
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct RangeRecord
    {
        public byte Bank;
        public ushort StartAddress;
        public ushort EndAddress;
        public byte Type;
    }

    // Can't convert this to struct because Name can be any length...
    public struct SymbolRecord
    {
        public byte NameLength;
        public string Name;
        public byte Flag;
        public byte BankNum;
        public ushort Address;
    }

    public struct unk_14h_record
    {
        public byte NameLength;
        public string Name;
        public ushort Flag;
        public ushort Address;
        public ushort BankNum;
    }

    public struct IsxContents
    {
        public List<BinaryRecord> Binaries;
        public List<RangeRecord> Ranges;
        public List<SymbolRecord> Symbols;
        public List<unk_14h_record> Unk14s;
    }
    class Program
    {
        static int BankSize = 0x4000; // GB bank size

        static void PrintUsage()
        {
            Console.WriteLine("usage: isxman [-rom] [-q] (path/to/isx)");
            Console.WriteLine("  -q: only print oversized banks");
            Console.WriteLine("  -rom: convert isx to rom file (path.gb) & sym file");
            Console.WriteLine("  -z: use 00 padding instead of FF");
        }
        static void Main(string[] args)
        {
            bool quiet = false;
            bool makerom = false;
            bool zeropad = false;

            string path = string.Empty;
            foreach(var arg in args)
            {
                if (arg == "-rom")
                    quiet = makerom = true;
                else if (arg == "-q")
                    quiet = true;
                else if (arg == "-z")
                    zeropad = true;
                else
                    path = arg;
            }

            if(string.IsNullOrEmpty(path))
            {
                PrintUsage();
                return;
            }

            // IsxContents used to allow easy exporting as JSON/XML/whatever, add that yourself if you want
            var contents = new IsxContents();
            contents.Binaries = new List<BinaryRecord>();
            contents.Ranges = new List<RangeRecord>();
            contents.Symbols = new List<SymbolRecord>();
            contents.Unk14s = new List<unk_14h_record>();

            var binData = new List<Tuple<long, byte[]>>();

            // for debug
            var binPos = new Dictionary<long, BinaryRecord>();
            var rangePos = new Dictionary<long, RangeRecord>();
            var symPos = new Dictionary<long, SymbolRecord>();

            var bankSizes = new int[128]; // hopefully there won't be anything using more than 128 banks...

            using (var reader = new BinaryReader(File.OpenRead(path)))
            {
                uint magic = reader.ReadUInt32();
                reader.BaseStream.Position -= 4;
                if (magic == 0x20585349)
                    reader.BaseStream.Position = 0x20; // skip ISX header from newer ISLK

                while (reader.BaseStream.Length > reader.BaseStream.Position)
                {
                    var position = reader.BaseStream.Position;
                    RecordType type = (RecordType)reader.ReadByte();
                    switch (type)
                    {
                        case RecordType.Binary:
                            {
                                var record = reader.ReadStruct<BinaryRecord>();
                                var recordData = reader.ReadBytes(record.Length);

                                int addr = record.Address;
                                if (addr >= BankSize)
                                    addr -= BankSize;

                                if (record.BankNum >= bankSizes.Length)
                                    Array.Resize(ref bankSizes, record.BankNum + 1);

                                if (addr + record.Length > bankSizes[record.BankNum])
                                    bankSizes[record.BankNum] = addr + record.Length;

                                addr += (record.BankNum * BankSize);

                                binData.Add(new Tuple<long, byte[]>(addr, recordData));

                                contents.Binaries.Add(record);
                                binPos.Add(position, record);
                                break;
                            }
                        case RecordType.Range:
                            {
                                var count = reader.ReadUInt16();

                                for (int i = 0; i < count; i++)
                                {
                                    position = reader.BaseStream.Position;
                                    var record = reader.ReadStruct<RangeRecord>();
                                    contents.Ranges.Add(record);
                                    rangePos.Add(position, record);
                                }
                                break;
                            }
                        case RecordType.Symbol:
                            {
                                var count = reader.ReadUInt16();

                                for (int i = 0; i < count; i++)
                                {
                                    position = reader.BaseStream.Position;

                                    var record = new SymbolRecord();
                                    record.NameLength = reader.ReadByte();
                                    record.Name = Encoding.ASCII.GetString(reader.ReadBytes(record.NameLength));
                                    record.Flag = reader.ReadByte();
                                    record.BankNum = reader.ReadByte();
                                    record.Address = reader.ReadUInt16();

                                    contents.Symbols.Add(record);
                                    symPos.Add(position, record);
                                }
                                break;
                            }
                        case RecordType.Unk14:
                            {
                                var count = reader.ReadUInt16();

                                for (int i = 0; i < count; i++)
                                {
                                    position = reader.BaseStream.Position;

                                    var record = new unk_14h_record();
                                    record.NameLength = reader.ReadByte();
                                    record.Name = Encoding.ASCII.GetString(reader.ReadBytes(record.NameLength));
                                    record.Flag = reader.ReadUInt16();
                                    record.Address = reader.ReadUInt16();
                                    record.BankNum = reader.ReadUInt16();

                                    contents.Unk14s.Add(record);
                                    // symPos.Add(position, record);
                                }
                                break;
                            }
                        case RecordType.Unk13:
                            {
                                var count = reader.ReadUInt16();
                                // skip this crap
                                reader.BaseStream.Position += (count * 9);
                                break;
                            }
                        case RecordType.Unk20:
                        case RecordType.Unk21:
                        case RecordType.Unk22:
                            {
                                var size = reader.ReadUInt32();
                                reader.BaseStream.Position += size;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine($"Unknown record type 0x{type:X}, prepare to crash!");
                                type = type;
                                break;
                            }
                    }
                }
            }

            // Calc bank sizes
            var largestBank = 0;
            foreach (var bin in contents.Binaries)
                if (bin.BankNum > largestBank)
                    largestBank = bin.BankNum;

            largestBank++;

            for (int i = 0; i < largestBank; i++)
            {
                var size = bankSizes[i];
                string oversized = size > BankSize ? $"\t-{size - BankSize:X} (oversized!)" : $"\t{BankSize - size:X}";
                if (!quiet || size > BankSize)
                    Console.WriteLine($"bank{i}:\t{i * BankSize:X}\t{size:X}/4000{oversized}");
            }

            if(makerom)
            {
                // Write out .sym file (only contains functions marked public :()
                if (contents.Unk14s.Count > 0 || contents.Symbols.Count > 0)
                {
                    var symPath = Path.ChangeExtension(path, ".sym");
                    var symbols = new List<string>();

                    foreach (var sym in contents.Unk14s)
                        symbols.Add($"{sym.BankNum:x2}:{sym.Address:x4} {sym.Name}");
                    foreach (var sym in contents.Symbols)
                        symbols.Add($"{sym.BankNum:x2}:{sym.Address:x4} {sym.Name}");
                    File.WriteAllLines(symPath, symbols.ToArray());
                }

                var outPath = Path.ChangeExtension(path, ".gb");
                using (var writer = new BinaryWriter(File.Create(outPath)))
                {
                    var emptyBank = new byte[BankSize];
                    for (int i = 0; i < BankSize; i++)
                        emptyBank[i] = (byte)(zeropad ? 0 : 0xFF);

                    // Pad file with 0xFF up to final bank
                    for (int i = 0; i < largestBank; i++)
                        writer.Write(emptyBank);

                    foreach(var bin in binData)
                    {
                        writer.BaseStream.Position = bin.Item1;
                        writer.Write(bin.Item2);
                    }

                    // fix checksum
                    ushort globalCksum = 0;
                    writer.BaseStream.Position = 0;
                    while(writer.BaseStream.Length > writer.BaseStream.Position)
                    {
                        if (writer.BaseStream.Position == 0x14E)
                            writer.BaseStream.Position += 2;
                        else
                        {
                            var byt = (byte)writer.BaseStream.ReadByte();
                            globalCksum += byt;
                        }
                    }
                    writer.BaseStream.Position = 0x14E;
                    writer.Write((byte)(globalCksum >> 8));
                    writer.Write((byte)(globalCksum & 0xFF));
                }
                Console.WriteLine($"Wrote ROM to {outPath}");
            }
        }
    }

    public static class Utility
    {
        public static T BytesToStruct<T>(byte[] bytes)
        {
            // Pin the managed memory while, copy it out the data, then unpin it
            var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            var theStructure = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();

            return theStructure;
        }
        public static T ReadStruct<T>(this BinaryReader reader)
        {
            var size = Marshal.SizeOf(typeof(T));

            // Read in a byte array
            byte[] bytes = reader.ReadBytes(size);

            return BytesToStruct<T>(bytes);
        }
    }
}
